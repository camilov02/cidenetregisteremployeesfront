import 'package:cidenet_register_employees/pages/employee_list.dart';
import 'package:cidenet_register_employees/pages/employee_save_edit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: ListEmployee.ROUTE,
      routes: {
        ListEmployee.ROUTE: (_) => ListEmployee(),
        EmployeeSaveEditPage.ROUTE: (_) => EmployeeSaveEditPage()
      },
    );
  }
}

