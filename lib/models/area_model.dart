class AreaModel {
  late final int id;
  late final String name;
  
  AreaModel({
    required this.id,
    required this.name
  });

  factory AreaModel.fromJson(Map<String, dynamic> json) {
    return AreaModel(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() => 
  {
      'id': id,
      'name': name
  };
}