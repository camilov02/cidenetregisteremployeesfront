import 'area_model.dart';

class EmployeeByAreaModel {
  late final int? id;
  late final DateTime dateAdmission;
  late final AreaModel area;

  
  
  EmployeeByAreaModel({
    this.id,
    required this.dateAdmission,
    required this.area
  });

  factory EmployeeByAreaModel.fromJson(Map<String, dynamic> json) {
    return EmployeeByAreaModel(
      id: json['id'],
      dateAdmission: DateTime.parse(json['dateAdmission']),
      area: AreaModel.fromJson(json['area']),
    );
  }

  Map<String, dynamic> toJson() => 
  {
      'id': id,
      'dateAdmission': dateAdmission.toIso8601String().split('T').first,
      'area': area.toJson()
  };
}