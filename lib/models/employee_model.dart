import 'dart:convert';

import 'package:cidenet_register_employees/models/country_model.dart';
import 'package:cidenet_register_employees/models/employee_by_area_model.dart';

import 'identification_type_model.dart';

class EmployeeModel {
  final int? id;
  final String firstLastName;
  final String secondLastName;
  final String firstName;
  final String? secondName;
  final IdentificationTypeModel identificationType;
  final String identification;
  final String? email;
  final EmployeeByAreaModel employeeArea;
  final CountryModel country; 
  
  EmployeeModel({
    required this.id,
    required this.firstLastName,
    required this.secondLastName,
    required this.firstName,
    this.secondName,
    required this.identificationType,
    required this.identification,
    this.email,
    required this.employeeArea,
    required this.country
  });

  factory EmployeeModel.fromJson(Map<dynamic, dynamic> json) {
    return EmployeeModel(
      id: json['id'],
      firstLastName: json['firstLastName'],
      secondLastName: json['secondLastName'],
      firstName: json['firstName'],
      secondName: json['secondName'],
      identificationType: IdentificationTypeModel.fromJson(json['identificationType']),
      identification: json['identification'],
      email: json['email'],
      employeeArea: EmployeeByAreaModel.fromJson(json['employeeArea']),
      country: CountryModel.fromJson(json['country'])
    );
  }

  Map<String, dynamic> toJson() => 
  {
      'id': id,
      'firstLastName': firstLastName,
      'secondLastName': secondLastName,
      'firstName': firstName,
      'secondName': secondName,
      'identificationType': identificationType.toJson(),
      'identification': identification,
      'email': email,
      'employeeArea': employeeArea.toJson(),
      'country': country.toJson()
  };
}

