class IdentificationTypeModel {
  final int? id;
  final String? name;
  
  IdentificationTypeModel({
    this.id,
    this.name
  });

  factory IdentificationTypeModel.fromJson(Map<String, dynamic> json) {
    return IdentificationTypeModel(
      id: json['id'],
      name: json['name'],
    );
  }

  Map<String, dynamic> toJson() => 
  {
      'id': id,
      'name': name
  };
}