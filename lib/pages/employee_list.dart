import 'package:cidenet_register_employees/models/employee_model.dart';
import 'package:cidenet_register_employees/pages/employee_save_edit.dart';
import 'package:cidenet_register_employees/providers/employee_service.dart';
import 'package:flutter/material.dart';


class ListEmployee extends StatefulWidget {
  static const String ROUTE = "/";

  @override
  _ListEmployeeState createState() => _ListEmployeeState();
}

class _ListEmployeeState extends State<ListEmployee> {

  late Future<List<EmployeeModel>> _futureEmployee;
  late Future<Map<String, String>> _futureEmployeeDelete;

  late Map<String, String>_futureEmployeeDeleted;

  @override
  void initState() {
    super.initState();
    _futureEmployee = fetchEmployees();
  }

  void _showDialog(BuildContext context, Map<String, String> result) {
    // flutter defined function

    String? state = result["state"];
    String? message = result["message"];

    showDialog(
      context: context,
      useRootNavigator: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(state!),
          content: new Text(message!),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: () {
                if(state == 'success') {
                  Navigator.pop(context); 
                  setState(() {
                    _futureEmployee = fetchEmployees();  
                  });
                }else {
                 Navigator.pop(context); 
                }
              },
            ),
          ],
        );
      },
    );
  }

  _deleteEmployee(int?  id) async {
    _futureEmployeeDeleted = await deleteEmployee(id);
    _showDialog(context, _futureEmployeeDeleted);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      floatingActionButton: FloatingActionButton(child: Icon(Icons.add), onPressed: () {
        Navigator.pushNamed(context, EmployeeSaveEditPage.ROUTE).then((value) => {
          setState(() {
            _futureEmployee = fetchEmployees();
          })
        });
      },),
      appBar: AppBar(title: Text("Empleados"),),
      body: Container(child: 
      FutureBuilder<List<EmployeeModel>>(
          future: _futureEmployee,
          builder: (context, snapshot) {
            if (!snapshot.hasData){
              return Center(child: CircularProgressIndicator());  
            } 
            return ListView(
              children: snapshot.data!.map((data) => Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.person),
                      title: Text(data.firstName + " " + data.firstLastName),
                      subtitle: Text(data.email! + "\n(" + data.employeeArea.area.name + ")"),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        TextButton(
                          child: const Text('ACTUALIZAR'),
                          onPressed: () {
                            Navigator.pushNamed(context, EmployeeSaveEditPage.ROUTE, arguments: data).then((value) => {
                              setState(() {
                                _futureEmployee = fetchEmployees();
                              })
                            });
                          },
                        ),
                        const SizedBox(width: 8),
                        TextButton(
                          child: const Text('ELIMINAR', style: TextStyle(color: Colors.red),),
                          onPressed: () {

                        showDialog<void>(
                            context: context,
                            barrierDismissible: false, // user must tap button!
                            builder: (BuildContext context) {
                            return AlertDialog(
                              title: const Text('Advertencia!'),
                              content: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    Text('Estas seguro de que deseas eliminar el empleado ' + data.firstName + ' ' + data.firstLastName),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                TextButton(
                                  child: const Text('Eliminar', style: TextStyle(color: Colors.red)),
                                  onPressed: () {
                                    _deleteEmployee(data.id);
                                    Navigator.of(context).pop();
                                  },
                                ),
                                TextButton(
                                  child: const Text('Cancelar'),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          });
                        }),
                        const SizedBox(width: 8),
                      ],
                    ),
                  ],
                )
              )
            ).toList()
          );
        })
      )
    );
  }
}