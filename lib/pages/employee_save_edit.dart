import 'package:cidenet_register_employees/models/area_model.dart';
import 'package:cidenet_register_employees/models/country_model.dart';
import 'package:cidenet_register_employees/models/employee_by_area_model.dart';
import 'package:cidenet_register_employees/models/employee_model.dart';
import 'package:cidenet_register_employees/models/identification_type_model.dart';
import 'package:cidenet_register_employees/pages/employee_list.dart';
import 'package:cidenet_register_employees/providers/country_services.dart';
import 'package:cidenet_register_employees/providers/area_services.dart';
import 'package:cidenet_register_employees/providers/employee_service.dart';
import 'package:cidenet_register_employees/providers/identification_type_services.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EmployeeSaveEditPage extends StatelessWidget {

  static const String ROUTE = "/save";
  
  
  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(title: Text("Guardar")),
      body: SingleChildScrollView(
        child: _FormSave(),
      ),
    );
  }
}

class _FormSave extends StatefulWidget {
  @override
  _FormSaveState createState() => _FormSaveState();
}

class _FormSaveState extends State<_FormSave> {

  final _globalKey = GlobalKey<FormState>();

  final firstNameController = TextEditingController();
  final secondNameController = TextEditingController();
  final firstLastnameController = TextEditingController();
  final secondLastnameController = TextEditingController();
  final identificationController = TextEditingController();
  final admissionDateController = TextEditingController();

  DateTime currentDate = DateTime.now();
  DateTime firstDate = new DateTime.now();

  late Future<List<IdentificationTypeModel>> _futureTypeIdentification;
  late Future<List<CountryModel>> _futureCountry;
  late Future<List<AreaModel>> _futureArea;
  late Map<String, String>_futureRegisterOrUpdate;

  late IdentificationTypeModel _identificationTypeSelected;
  late CountryModel _countrySelected;
  late AreaModel _areaSelected;
  
  late List<IdentificationTypeModel> _listIdentificationTypes = [];
  late List<CountryModel> _listCountries = [];
  late List<AreaModel> _listAreas = [];

  bool statusResponse = false;

  var _selectCountry = "";
  var _selectIdentificationType = "";
  var _selectArea = "";
  var _selectedtDate = DateTime.now();

  EmployeeModel? args = null;
  _setArgumentsForFields(EmployeeModel args) async {

    firstNameController.text = args.firstName;
    secondNameController.text = args.secondName!;
    firstLastnameController.text = args.firstLastName;
    secondLastnameController.text = args.secondLastName;
    identificationController.text = args.identification;
    _selectIdentificationType = args.identificationType.id.toString();
    _selectCountry = args.country.id.toString();
    _selectArea = args.employeeArea.area.id.toString();
    admissionDateController.text = dateFormat(args.employeeArea.dateAdmission);
    _selectedtDate = args.employeeArea.dateAdmission;
  }
  @override
  void initState() {
    super.initState();

    firstDate = DateTime(currentDate.year, currentDate.month - 1, currentDate.day);
    
    _futureCountry = getInitCountries();
    _futureArea = getInitAreas();
    _futureTypeIdentification = getInitIdentificationTypes();

    Future.delayed(Duration.zero, () {
      setState(() {
        if(ModalRoute.of(context)!.settings.arguments != null) {
          args = ModalRoute.of(context)!.settings.arguments as EmployeeModel;
        }
      });
      if(args!= null) _setArgumentsForFields(args!);
    });
  }

  Future<List<IdentificationTypeModel>> getInitIdentificationTypes() async {

    _listIdentificationTypes = await fetchIdentificationTypes();
    if(args == null) _selectIdentificationType = _listIdentificationTypes[0].id.toString();
    return _listIdentificationTypes;   
  }

  Future<List<CountryModel>> getInitCountries() async {
    _listCountries = await fetchCountries();
    if(args == null) _selectCountry = _listCountries[0].id.toString();
    return _listCountries;
  }

  Future<List<AreaModel>> getInitAreas() async {
    _listAreas = await fetchAreas();
    if(args == null) _selectArea = _listAreas[0].id.toString();
    return _listAreas;
  }

  Future<void> _selectDate(BuildContext context) async {
  final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: _selectedtDate,
      firstDate: firstDate,
      lastDate: currentDate);
  if (pickedDate != null && pickedDate != currentDate)
    setState(() {
      _selectedtDate = pickedDate;
      admissionDateController.text = dateFormat(currentDate);
    });
  }

  String dateFormat(DateTime date) {
    return DateFormat('yyyy-MM-dd').format(date);
  }

  IdentificationTypeModel getIdentificationTypeSelected(int id) {
    IdentificationTypeModel seletedIdType = _listIdentificationTypes.firstWhere((element) => element.id == id);
    return seletedIdType;
  }

  CountryModel getCountrySelected(int id) {
    CountryModel selectedCountry = _listCountries.firstWhere((element) => element.id == id);
    return selectedCountry;
  }

  AreaModel getAreaSelected(int id) {
    AreaModel selectedArea = _listAreas.firstWhere((element) => element.id == id);
    return selectedArea;
  }

  void _showDialog(BuildContext context, Map<String, String> result) {
    // flutter defined function

    String? state = result["state"];
    String? message = result["message"];

    showDialog(
      context: context,
      useRootNavigator: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(state!),
          content: new Text(message!),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok!"),
              onPressed: () {
                if(state == 'success') {
                  Navigator.pop(context, true);
                  Navigator.pop(context, true);
                }else {
                 Navigator.pop(context); 
                }
              },
            ),
          ],
        );
      },
    );
  }

  void saveEmployee(BuildContext context) async {

    _areaSelected = getAreaSelected(int.parse(_selectArea));
    _countrySelected = getCountrySelected(int.parse(_selectCountry));
    _identificationTypeSelected = getIdentificationTypeSelected(int.parse(_selectIdentificationType));

    int? idEmployee = args != null ? args!.id : null;
    int? idEmployeeByArea = args != null ? args!.employeeArea.id : null;

    EmployeeByAreaModel employeeByArea = new EmployeeByAreaModel
    (
      id: idEmployeeByArea, 
      dateAdmission: DateTime.parse(admissionDateController.text), 
      area: _areaSelected
    );

    EmployeeModel employeeModel = new EmployeeModel(
      id: idEmployee,
      firstName: firstNameController.text,
      secondName: secondNameController.text,
      firstLastName: firstLastnameController.text,
      secondLastName: secondLastnameController.text,
      identificationType: _identificationTypeSelected,
      identification: identificationController.text,
      email: null,
      country: _countrySelected,
      employeeArea: employeeByArea
    );

    
    if(args != null) {
      _futureRegisterOrUpdate = await updateEmployee(employeeModel);
    }else {
      _futureRegisterOrUpdate = await registerEmployee(employeeModel);
    }
    
    _showDialog(context, _futureRegisterOrUpdate);
}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(15),
      child: Form(
        key: _globalKey,
        child: Column(children: <Widget>[
        TextFormField(
          controller: firstNameController,
          maxLength: 20,
          validator: (value) {
            return value!.isEmpty ? "El primer nombre es obligatorio" : null;
          },
          decoration: InputDecoration(
            labelText: "Primer nombre",
            border: OutlineInputBorder(),
          )
        ),
        SizedBox(height: 15),
        TextFormField(
          controller: secondNameController,
          maxLength: 50,
          validator: (value) {
            return value!.isEmpty ? "El segundo nombre es obligatorio" : null;
          },
          decoration: InputDecoration(
            labelText: "Segundo nombre",
            border: OutlineInputBorder(),
          )
        ),
        SizedBox(height: 15),
        TextFormField(
          controller: firstLastnameController,
          maxLength: 20,
          validator: (value) {
            return value!.isEmpty ? "El primer apellido es obligatorio" : null;
          },
          decoration: InputDecoration(
            labelText: "Primer apellido",
            border: OutlineInputBorder(),
          )
        ),
        SizedBox(height: 15),
        TextFormField(
          controller: secondLastnameController,
          maxLength: 20,
          validator: (value) {
            return value!.isEmpty ? "El segundo apellido es obligatorio" : null;
          },
          decoration: InputDecoration(
            labelText: "Segundo apellido",
            border: OutlineInputBorder(),
          )
        ),
        SizedBox(height: 15),

        FutureBuilder<List<IdentificationTypeModel>>(
          future: _futureTypeIdentification,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());  
            }
              return DropdownButtonFormField<String>(
              decoration: InputDecoration(labelText: 'Tipo identifición'),
              value: _selectIdentificationType,
              onChanged: (newValue) {
                setState(() {
                  _selectIdentificationType = newValue!;
                });
              },
              items: snapshot.data!.map((idType) {
                return DropdownMenuItem<String>(
                  child: Text(idType.name!),
                  value: idType.id.toString(),
                );
              }).toList());
            
        }),

        SizedBox(height: 15),
        TextFormField(
          controller: identificationController,
          maxLength: 20,
          validator: (value) {
            return value!.isEmpty ? "La identificación es obligatoria" : null;
          },
          decoration: InputDecoration(
            labelText: "Identificación",
            border: OutlineInputBorder(),
          )
        ),
        SizedBox(height: 15),

        FutureBuilder<List<CountryModel>>(
          future: _futureCountry,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());  
            }
              return DropdownButtonFormField<String>(
              decoration: InputDecoration(labelText: 'País'),
              value: _selectCountry,
              onChanged: (newValue) {
                setState(() {
                  _selectCountry = newValue!;
                });
              },
              items: snapshot.data!.map((country) {
                return DropdownMenuItem<String>(
                  child: Text(country.name),
                  value: country.id.toString(),
                );
              }).toList());
            
        }),

        SizedBox(height: 15),

        FutureBuilder<List<AreaModel>>(
          future: _futureArea,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());  
            }
              return DropdownButtonFormField<String>(
              decoration: InputDecoration(labelText: 'Area'),
              value: _selectArea,
              onChanged: (newValue) {
                setState(() {
                  _selectArea = newValue!;
                });
              },
              items: snapshot.data!.map((area) {
                return DropdownMenuItem<String>(
                  child: Text(area.name),
                  value: area.id.toString(),
                );
              }).toList());
            
        }),

        SizedBox(height: 15),
        
        TextField(
          controller: admissionDateController,
          enableInteractiveSelection: false,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20.0)
            ),
            hintText: 'Fecha de admisión',
            labelText: 'Fecha de admisión',
            suffixIcon: Icon(Icons.perm_contact_calendar),
            icon: Icon(Icons.calendar_today)
          ),
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
            _selectDate(context);
          },
        ),
        
         SizedBox(height: 15),
         ElevatedButton(
          onPressed: () {
            if(_globalKey.currentState!.validate()) {
              saveEmployee(context);  

              if(statusResponse) {
                Navigator.pop(context);
              }
            }
          }, 
          child: Text("Guardar")) 
      ],
      )),
    );
  }
}





