import 'dart:convert';

import 'package:cidenet_register_employees/models/area_model.dart';
import 'package:http/http.dart' as http;

Future<List<AreaModel>> fetchAreas() async {
  final response =
      await http.get(Uri.parse('http://localhost:8081/api/area/list'));

  if (response.statusCode == 200) {
    return parseAreas(Utf8Decoder().convert(response.bodyBytes));
  } else {
    throw Exception('Failed to load album');
  }
}

List<AreaModel> parseAreas(String responseBody) { 
   final parsed = json.decode(responseBody).cast<Map<String, dynamic>>(); 
   return parsed.map<AreaModel>((json) =>AreaModel.fromJson(json)).toList(); 
} 