import 'dart:convert';

import 'package:cidenet_register_employees/models/country_model.dart';
import 'package:http/http.dart' as http;

Future<List<CountryModel>> fetchCountries() async {
  final response =
      await http.get(Uri.parse('http://localhost:8081/api/country/list'));

  if (response.statusCode == 200) {
    return parseCountries(response.body);
  } else {
    throw Exception('Failed to load album');
  }
}

List<CountryModel> parseCountries(String responseBody) { 
   final parsed = json.decode(responseBody).cast<Map<String, dynamic>>(); 
   return parsed.map<CountryModel>((json) =>CountryModel.fromJson(json)).toList(); 
} 