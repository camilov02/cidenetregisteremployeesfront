import 'dart:convert';

import 'package:cidenet_register_employees/models/employee_model.dart';
import 'package:http/http.dart' as http;

Future<List<EmployeeModel>> fetchEmployees() async {
  final response =
      await http.get(Uri.parse('http://localhost:8081/api/employee/list'));

  if (response.statusCode == 200) {
    return parseEmployees(Utf8Decoder().convert(response.bodyBytes));
  } else {
    throw Exception('Failed to load album');
  }
}

Future<Map<String, String>> registerEmployee(EmployeeModel employeeModel) async{
  
  try {
    var response = await http.post(
    Uri.parse('http://localhost:8081/api/employee/save'),
    headers: <String, String>{
      'Content-Type': 'application/json',
    },
      body: jsonEncode(employeeModel.toJson())
    );

    if(response.statusCode == 201) {
      return {"title": "Excelente", "message" : "El empleado se ha registrado correctamente", 'state': 'success'};
    }else {
      print("final error : jaym " +  response.body);
      return {"title" : "Error", "message" : Utf8Decoder().convert(response.bodyBytes), 'state': 'error'}; 
    }    
  }catch(error) {
    print("error : ${error.toString()}");
    return {"title" : "Error", "message" : "Hubo un error inesperado", 'state': 'error'};
  }
}

Future<Map<String, String>> updateEmployee(EmployeeModel employeeModel) async{
  print("Hola" + jsonEncode(employeeModel.toJson()));

  try {
    var response = await http.put(
    Uri.parse("http://localhost:8081/api/employee/update/${employeeModel.id}"),
    headers: <String, String>{
      'Content-Type': 'application/json',
    },
      body: jsonEncode(employeeModel.toJson())
    );

    if(response.statusCode == 200) {
      return {"title": "Excelente", "message" : "El empleado se ha actualizado correctamente", 'state': 'success'};
    }else {
      print("final error : jaym " +  response.body);
      return {"title" : "Error", "message" : Utf8Decoder().convert(response.bodyBytes), 'state': 'error'}; 
    }    
  }catch(error) {
    print("error : ${error.toString()}");
    return {"title" : "Error", "message" : "Hubo un error inesperado", 'state': 'error'}; 
  }
}

Future<Map<String, String>> deleteEmployee(int? id) async{
  
  try {
    var response = await http.delete(
    Uri.parse("http://localhost:8081/api/employee/remove/${id}"),
    headers: <String, String>{
      'Content-Type': 'application/json',
    });

    if(response.statusCode == 200) {
      return {"title": "Excelente", "message" : "El empleado se ha eliminado correctamente", 'state': 'success'};
    }else {
      print("final error : jaym " +  response.body);
      return {"title" : "Error", "message" : Utf8Decoder().convert(response.bodyBytes), 'state': 'error'}; 
    }    
  }catch(error) {
    print("error : ${error.toString()}");
    return {"title" : "Error", "message" : "Hubo un error inesperado", 'state': 'error'}; 
  }
}


List<EmployeeModel> parseEmployees(String responseBody) { 
   final parsed = json.decode(responseBody).cast<Map<String, dynamic>>(); 
   return parsed.map<EmployeeModel>((json) =>EmployeeModel.fromJson(json)).toList(); 
}

String formatErrors(List<String> listErrors) {
  String error = "";
  for (var item in listErrors) {
    error = "-" + item + "\n";
  }

  return error;
}

