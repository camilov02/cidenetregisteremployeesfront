import 'dart:convert';

import 'package:cidenet_register_employees/models/identification_type_model.dart';
import 'package:http/http.dart' as http;

Future<List<IdentificationTypeModel>> fetchIdentificationTypes() async {
  final response =
      await http.get(Uri.parse('http://localhost:8081/api/identification-type/list'));

  if (response.statusCode == 200) {
    return parseIdentificationTypes(Utf8Decoder().convert(response.bodyBytes));
  } else {
    throw Exception('Failed to load album');
  }
}

List<IdentificationTypeModel> parseIdentificationTypes(String responseBody) { 
   final parsed = json.decode(responseBody).cast<Map<String, dynamic>>(); 
   return parsed.map<IdentificationTypeModel>((json) =>IdentificationTypeModel.fromJson(json)).toList(); 
} 